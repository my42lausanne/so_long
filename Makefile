# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2021/12/06 15:42:47 by dfarhi           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FILES		= so_long read_mapfile read_mapfile_utils tab_utils mlx_window	\
			  window_utils draw_frame movement

SRCS_TEMP	= $(addprefix src/, ${FILES})
SRCS		= $(addsuffix .c, ${SRCS_TEMP})
OBJS		= ${SRCS:.c=.o}

NAME		= so_long
CC			= gcc -Wall -Wextra -Werror

AR			= ar rcs

INCLUDES	= -I./includes -I./libft/includes
INCLUDE_MLX =

LOCAL_LIBS	= -L./libs/ -lft -lmlx

LIBFT		= libs/libft.a
MINILIBX	= libs/libmlx.a

UNAME		= $(shell uname)
LINUX_LIBS	= -lXext -lX11
MACOS_LIBS	= -framework OpenGL -framework AppKit

ifeq ($(UNAME), Linux)
LIBS := ${LOCAL_LIBS} ${LINUX_LIBS}
INCLUDE_MLX	:= -I./minilibx_linux
else
LIBS := ${LOCAL_LIBS} ${MACOS_LIBS}
INCLUDE_MLX	:= -I./minilibx_macos
endif

.c.o:
			${CC} -c ${INCLUDES} ${INCLUDE_MLX} $< -o ${<:.c=.o}

${NAME}:	${MINILIBX} ${LIBFT} ${OBJS}
			${CC} ${INCLUDES} ${INCLUDE_MLX} -o ${NAME} ${OBJS} ${LIBS}

all:		${NAME}

${LIBFT}:
			$(MAKE) -C ./libft get-next-line
			cp ./libft/libft.a ${LIBFT}

ifeq ($(UNAME), Linux)
minilibx_clean:
			$(MAKE) -C ./minilibx_linux clean
${MINILIBX}:
			$(MAKE) -C ./minilibx_linux
			cp minilibx_linux/libmlx.a ${MINILIBX}
else
minilibx_clean:
			$(MAKE) -C ./minilibx_macos clean
${MINILIBX}:
			$(MAKE) -C ./minilibx_macos 2> /dev/null
			cp minilibx_macos/libmlx.a ${MINILIBX}
endif

platform:
			@echo "Platform: ${UNAME}"
			@echo "Libs variable: ${LIBS}"

git:
			git submodule update --init --recursive

clean:		minilibx_clean
			rm -f ${OBJS}
			$(MAKE) -C ./libft fclean

# "Force clean" remove all compiled files
fclean:		clean
			rm -f ${NAME} ${LIBFT} ${MINILIBX_LINUX} ${MINILIBX_MACOS}

# Rule to recompile
re:			fclean all

# Rules that do not have files, if those files exist, they are ignored
.PHONY:		all clean fclean re
