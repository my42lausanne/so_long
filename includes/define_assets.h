/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define_assets.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 20:40:05 by davifah           #+#    #+#             */
/*   Updated: 2021/12/04 21:20:21 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_ASSETS_H
# define DEFINE_ASSETS_H

# define WALL "assets/tiles/bush.xpm"
# define COLL "assets/tiles/flowers.xpm"
# define EMPTY "assets/tiles/ground.xpm"
# define EXIT "assets/tiles/vase.xpm"
# define PLAYER "assets/character/elf.xpm"

#endif
