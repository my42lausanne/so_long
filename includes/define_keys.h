/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define_keys.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 20:19:08 by davifah           #+#    #+#             */
/*   Updated: 2021/12/06 15:13:42 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_KEYS_H
# define DEFINE_KEYS_H

// KEYCODES FOR LINUX
/*
# define ESCAPE_KEY 65509
# define W_KEY 119
# define A_KEY 97
# define S_KEY 115
# define D_KEY 100
*/

// KEYCODES FOR MACOS
# define ESCAPE_KEY 53
# define W_KEY 13
# define A_KEY 0
# define S_KEY 1
# define D_KEY 2

#endif
