/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 14:12:27 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/05 11:58:50 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include "expanded.h"
# include "get_next_line.h"
# include "mlx.h"
# include <stdio.h>
# include <unistd.h>

# define PIXEL_SCALE 4

typedef struct s_data
{
	void	*img;
	char	*addr;
	int		bpp;
	int		line_len;
	int		endian;
}	t_data;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
	void	*wall;
	void	*coll;
	void	*empty;
	void	*exit;
	void	*player;
	t_data	img;
	char	**tab;
	int		win_width;
	int		win_height;
	int		moves;
	int		ended;
}	t_mlx;

void	ft_error(char *msg);
int		ft_error_noexit(char *msg);
char	**read_mapfile(char *filename);
void	free_tab(char **tab);
int		tablen(char **tab);
int		setup_mlx(char **tab);
int		print_key(int key, void *param);
void	ft_pixel_put(t_mlx *mlx, int x, int y, unsigned int color);
void	fill_image(t_mlx *mlx, unsigned int color);
void	draw_frame(t_mlx *mlx);
int		count_char(char **tab, char chr);
void	move_player(t_mlx *mlx, int x_move, int y_move);

#endif
