/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_frame.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 21:44:44 by davifah           #+#    #+#             */
/*   Updated: 2021/12/05 11:40:24 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

static void	ft_put_image(t_mlx *mlx, void *image, int i, int j)
{
	mlx_put_image_to_window(mlx->mlx, mlx->win, image, 16 * PIXEL_SCALE * j,
		16 * PIXEL_SCALE * i);
}

void	draw_frame(t_mlx *mlx)
{
	int	i;
	int	j;

	i = -1;
	while (++i < tablen(mlx->tab))
	{
		j = -1;
		while ((unsigned long)++j < ft_strlen(mlx->tab[0]))
		{
			if (mlx->tab[i][j] == '1')
				ft_put_image(mlx, mlx->wall, i, j);
			else if (mlx->tab[i][j] == '0')
				ft_put_image(mlx, mlx->empty, i, j);
			else if (mlx->tab[i][j] == 'C')
				ft_put_image(mlx, mlx->coll, i, j);
			else if (mlx->tab[i][j] == 'E')
				ft_put_image(mlx, mlx->exit, i, j);
			else if (mlx->tab[i][j] == 'P')
				ft_put_image(mlx, mlx->player, i, j);
		}
	}
	printf("Move counter: %d\n", mlx->moves);
}
