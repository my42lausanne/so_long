/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_window.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 17:18:27 by davifah           #+#    #+#             */
/*   Updated: 2021/12/06 15:57:09 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"
#include "define_keys.h"
#include "define_assets.h"

static int	deal_key(int key, void *param);
static void	load_images(t_mlx *mlx);
static int	win_close(int code, void *param);
int			key_escape(void *param);

int	setup_mlx(char **tab)
{
	t_mlx	mlx;

	mlx.tab = tab;
	mlx.moves = 0;
	mlx.ended = 0;
	mlx.win_width = ft_strlen(tab[0]) * 16 * PIXEL_SCALE;
	mlx.win_height = tablen(tab) * 16 * PIXEL_SCALE;
	mlx.mlx = mlx_init();
	mlx.win = mlx_new_window(mlx.mlx, mlx.win_width, mlx.win_height, "so_long");
	mlx_hook(mlx.win, 17, 1L << 0, win_close, (void *)&mlx);
	mlx_key_hook(mlx.win, deal_key, (void *)&mlx);
	mlx.img.img = mlx_new_image(mlx.mlx, mlx.win_width, mlx.win_height);
	mlx.img.addr = mlx_get_data_addr(mlx.img.img, &mlx.img.bpp,
			&mlx.img.line_len, &mlx.img.endian);
	load_images(&mlx);
	draw_frame(&mlx);
	mlx_put_image_to_window(mlx.mlx, mlx.win, mlx.wall, 0, 0);
	mlx_loop(mlx.mlx);
	return (0);
}

static void	load_images(t_mlx *mlx)
{
	int	width;
	int	height;

	mlx->wall = mlx_xpm_file_to_image(mlx->mlx, WALL, &width, &height);
	mlx->coll = mlx_xpm_file_to_image(mlx->mlx, COLL, &width, &height);
	mlx->empty = mlx_xpm_file_to_image(mlx->mlx, EMPTY, &width, &height);
	mlx->exit = mlx_xpm_file_to_image(mlx->mlx, EXIT, &width, &height);
	mlx->player = mlx_xpm_file_to_image(mlx->mlx, PLAYER, &width, &height);
}

static int	deal_key(int key, void *param)
{
	t_mlx	*mlx;

	mlx = (t_mlx *)param;
	if (key == ESCAPE_KEY)
		key_escape(param);
	else if (key == W_KEY)
		move_player((t_mlx *) param, 0, -1);
	else if (key == A_KEY)
		move_player((t_mlx *) param, -1, 0);
	else if (key == S_KEY)
		move_player((t_mlx *) param, 0, 1);
	else if (key == D_KEY)
		move_player((t_mlx *) param, 1, 0);
	(void)mlx;
	return (0);
}

static int	win_close(int code, void *param)
{
	printf("Quitting program...\n");
	exit(0);
	(void)param;
	(void)code;
	return (0);
}
