/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movement.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 22:29:53 by davifah           #+#    #+#             */
/*   Updated: 2021/12/05 12:03:05 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

typedef struct s_position
{
	int	x;
	int	y;
}	t_position;

static t_position	get_ppos(char **tab)
{
	t_position	pos;

	pos.y = 0;
	while (pos.y < tablen(tab))
	{
		pos.x = 0;
		while ((unsigned long)pos.x < ft_strlen(tab[0]))
		{
			if (tab[pos.y][pos.x] == 'P')
				return (pos);
			pos.x++;
		}
		pos.y++;
	}
	return (pos);
}

void	move_player(t_mlx *mlx, int x_move, int y_move)
{
	t_position	pos;
	char		c;

	pos = get_ppos(mlx->tab);
	c = mlx->tab[pos.y + y_move][pos.x + x_move];
	if (mlx->ended || c == '1' || (c == 'E' && count_char(mlx->tab, 'C')))
		return ;
	mlx->moves++;
	mlx->tab[pos.y][pos.x] = '0';
	mlx->tab[pos.y + y_move][pos.x + x_move] = 'P';
	draw_frame(mlx);
	if (c == 'E')
	{
		mlx->ended = 1;
		printf("Congrats, you won the game\nPress ESC to quit\n");
	}
}	
