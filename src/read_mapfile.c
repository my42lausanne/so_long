/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_mapfile.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 14:43:31 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/04 17:13:09 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"
#include <fcntl.h>
#include <sys/errno.h>
#include <string.h>

static void	verify_mapfile(t_list *lst);
static int	verify_mapline(char *line, int linenum, int lstlen, int *chars);
static int	lst_isrect(t_list *lst, int len);
char		**lst_to_tab(t_list *lst);
void		free_tab(char **tab);
void		reset_chars(int *c0, int *c1, int *c2);
void		replace_endl(void *str);
void		ft_clear_error(t_list *lst, char *msg);

char	**read_mapfile(char *filename)
{
	t_list	*lst;
	char	*line;
	int		fd;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
		ft_error(strerror(errno));
	lst = NULL;
	line = get_next_line(fd);
	while (line)
	{
		if (!lst)
			lst = ft_lstnew(ft_strdup(line));
		else
			ft_lstadd_back(&lst, ft_lstnew(ft_strdup(line)));
		free(line);
		line = get_next_line(fd);
	}
	if (close(fd) < 0)
	{
		ft_lstclear(&lst, free);
		ft_error(strerror(errno));
	}
	verify_mapfile(lst);
	return (lst_to_tab(lst));
}

static void	verify_mapfile(t_list *lst)
{
	t_list	*needle;
	int		counter;
	int		chars[3];

	reset_chars(&chars[0], &chars[1], &chars[2]);
	ft_lstiter(lst, replace_endl);
	counter = 0;
	needle = lst;
	while (needle)
	{
		if (verify_mapline(needle->content, counter, ft_lstsize(lst), chars))
			ft_clear_error(lst, "");
		counter++;
		needle = needle->next;
	}
	if (!chars[0] || !chars[1] || !chars[2])
		ft_clear_error(lst,
			"Map doesn't have at least one exit, collectible or player");
	if (!lst_isrect(lst, -1))
		ft_clear_error(lst, "Map is not rectangular");
}

// "chars[0]": collectible
// "chars[1]": exit
// "chars[2]": player
static int	verify_mapline(char *line, int linenum, int lstlen, int *chars)
{
	int	i;

	i = -1;
	if (!line)
		ft_error("Memory allocation problem");
	while (line[++i])
	{
		if ((i == 0 || !line[i + 1] || linenum == 0
				|| linenum == lstlen - 1) && line[i] != '1')
			return (ft_error_noexit("The map is not surrounded by walls"));
		if (!ft_strchr("01CEP", line[i]))
			return (ft_error_noexit("Undefined character found in map"));
		if (line[i] == 'C')
			chars[0]++;
		else if (line[i] == 'E')
			chars[1]++;
		else if (line[i] == 'P')
			chars[2]++;
	}
	if (chars[2] > 1)
		return (ft_error_noexit("Map has more than one player"));
	return (0);
}

static int	lst_isrect(t_list *lst, int len)
{
	if (len >= 0 && ft_strlen(lst->content) != (unsigned long) len)
		return (0);
	if (!lst->next)
		return (1);
	return (lst_isrect(lst->next, ft_strlen(lst->content)));
}
