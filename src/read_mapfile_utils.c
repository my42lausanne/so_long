/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_mapfile_utils.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 13:36:09 by davifah           #+#    #+#             */
/*   Updated: 2021/12/04 17:27:44 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	reset_chars(int *c0, int *c1, int *c2)
{
	*c0 = 0;
	*c1 = 0;
	*c2 = 0;
}

void	replace_endl(void *str)
{
	int		i;
	char	*ptr;

	ptr = (char *)str;
	i = -1;
	while (ptr[++i])
		if (ptr[i] == '\n')
			ptr[i] = 0;
}

void	ft_clear_error(t_list *lst, char *msg)
{
	ft_lstclear(&lst, free);
	if (msg[0])
		ft_error(msg);
	exit(1);
}
