/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 14:15:00 by dfarhi            #+#    #+#             */
/*   Updated: 2021/12/05 11:40:56 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

void	print_tab(char **tab)
{
	int	i;

	i = -1;
	while (tab[++i])
		printf("line %d - '%s'\n", i, tab[i]);
}

int	main(int ac, char **av)
{
	char	**tab;

	if (ac < 2)
		ft_error("Map file must be sent as argument");
	tab = read_mapfile(av[1]);
	if (!tab)
		ft_error("Memory allocation problem");
	setup_mlx(tab);
}

void	ft_error(char *msg)
{
	ft_error_noexit(msg);
	exit(1);
}

int	ft_error_noexit(char *msg)
{
	ft_putendl_fd("Error", 1);
	ft_putendl_fd(msg, 1);
	return (1);
}
