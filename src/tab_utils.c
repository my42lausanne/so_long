/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 17:24:00 by davifah           #+#    #+#             */
/*   Updated: 2021/12/06 15:32:24 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

char	**lst_to_tab(t_list *lst)
{
	char	**tab;
	t_list	*needle;
	int		i;

	tab = ft_calloc(ft_lstsize(lst) + 1, sizeof(char *));
	if (!tab)
		return (0);
	i = -1;
	needle = lst;
	while (++i < ft_lstsize(lst) && needle)
	{
		tab[i] = ft_strdup(needle->content);
		if (!tab[i])
			return (0);
		needle = needle->next;
	}
	ft_lstclear(&lst, free);
	return (tab);
}

void	free_tab(char **tab)
{
	int	i;

	i = -1;
	while (++i < tablen(tab))
		free(tab[i]);
	free(tab);
}

int	tablen(char **tab)
{
	int	c;

	c = -1;
	while (tab[++c])
		continue ;
	return (c);
}

int	count_char(char **tab, char chr)
{
	int	i;
	int	j;
	int	c;

	c = 0;
	i = -1;
	while (tab[++i])
	{
		j = -1;
		while (tab[i][++j])
			if (tab[i][j] == chr)
				c++;
	}
	return (c);
}
