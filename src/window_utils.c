/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/04 17:38:37 by davifah           #+#    #+#             */
/*   Updated: 2021/12/06 15:50:47 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	print_key(int key, void *param)
{
	(void)param;
	printf("Key: %d\n", key);
	return (0);
}

void	fill_image(t_mlx *mlx, unsigned int color)
{
	int	i;
	int	j;

	i = -1;
	while (++i < mlx->win_width)
	{
		j = -1;
		while (++j < mlx->win_height)
			ft_pixel_put(mlx, i, j, color);
	}
}

void	ft_pixel_put(t_mlx *mlx, int x, int y, unsigned int color)
{
	char	*dst;

	dst = mlx->img.addr + (y * mlx->img.line_len + x * (mlx->img.bpp / 8));
	*(unsigned int *)dst = color;
}

// possible source for segfault: freetab
int	key_escape(void *param)
{
	free_tab(((t_mlx *)param)->tab);
	if (!((t_mlx *)param)->ended)
		printf("Escape pressed, quitting program...\n");
	mlx_destroy_window(((t_mlx *)param)->mlx, ((t_mlx *)param)->win);
	exit(0);
	return (0);
}
